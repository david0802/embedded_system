
// Program to Display LCD on MBED Application Board
#include  "LPC17xx.h"						// Include this as part of CMSIS
#include  <stdio.h>
#include  "spi.h"
#include  "lcd.h"
				
int main( ) {	 
	unsigned int data;
	
	spi_init( );	// Initialise SPI
	lcd_init( );	// Initialise LCD
	lcd_clear( );	// Clear LCD Screen
	
	data = 43981;
	while(1) {
		lcd_locate(0,0);			// Locate LCD to line 0 and column 0
		printf("Hello World \n");
		printf("%x \n",data);	
	}	 
} 	 

